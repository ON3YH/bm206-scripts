-- MySQL dump 10.13  Distrib 5.5.47, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: bmconfig
-- ------------------------------------------------------
-- Server version	5.5.47-0+deb8u1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activityLogTypes`
--

DROP TABLE IF EXISTS `activityLogTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activityLogTypes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `activityLogs`
--

DROP TABLE IF EXISTS `activityLogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activityLogs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `sysopId` int(11) DEFAULT NULL,
  `repeaterId` int(11) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `ip` varchar(64) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=664 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `callsigns`
--

DROP TABLE IF EXISTS `callsigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `callsigns` (
  `radioId` int(11) NOT NULL DEFAULT '0',
  `callsign` varchar(50) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`radioId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `groupName` varchar(32) NOT NULL,
  `groupId` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`groupId`),
  UNIQUE KEY `groupName` (`groupName`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lastHeard`
--

DROP TABLE IF EXISTS `lastHeard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lastHeard` (
  `dmrId` int(11) NOT NULL DEFAULT '0',
  `repeaterId` int(11) DEFAULT NULL,
  `slot` tinyint(4) DEFAULT NULL,
  `destId` int(11) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `time` bigint(20) DEFAULT NULL,
  `sessionId` varchar(50) DEFAULT NULL,
  `rssi` int(11) DEFAULT NULL,
  `dataCount` int(11) DEFAULT NULL,
  PRIMARY KEY (`dmrId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `onDemandGroups`
--

DROP TABLE IF EXISTS `onDemandGroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `onDemandGroups` (
  `dmrId` int(11) NOT NULL DEFAULT '0',
  `groupName` varchar(50) DEFAULT NULL,
  `timeSlot` tinyint(4) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`dmrId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `regions`
--

DROP TABLE IF EXISTS `regions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `regions` (
  `dmrId` int(11) NOT NULL DEFAULT '0',
  `region` tinyint(4) DEFAULT NULL,
  `regionName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`dmrId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `repeaterAllowOnDemand`
--

DROP TABLE IF EXISTS `repeaterAllowOnDemand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repeaterAllowOnDemand` (
  `repeaterId` int(11) NOT NULL DEFAULT '0',
  `allow` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`repeaterId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `repeaterAllowReflector`
--

DROP TABLE IF EXISTS `repeaterAllowReflector`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repeaterAllowReflector` (
  `repeaterId` int(11) NOT NULL DEFAULT '0',
  `allow` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`repeaterId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `repeaterGroupSubscription`
--

DROP TABLE IF EXISTS `repeaterGroupSubscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repeaterGroupSubscription` (
  `repeaterId` int(11) NOT NULL DEFAULT '0',
  `member` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`repeaterId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `repeaterRegionSubscriptions`
--

DROP TABLE IF EXISTS `repeaterRegionSubscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repeaterRegionSubscriptions` (
  `repeaterId` int(11) NOT NULL DEFAULT '0',
  `regions` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`repeaterId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `repeaterSysops`
--

DROP TABLE IF EXISTS `repeaterSysops`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repeaterSysops` (
  `repeaterId` int(11) NOT NULL,
  `sysopId` int(11) NOT NULL,
  `sendTextOnOutage` tinyint(1) DEFAULT '0',
  `sendMailOnOutage` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`repeaterId`,`sysopId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `repeaterTalkGroupSubscription`
--

DROP TABLE IF EXISTS `repeaterTalkGroupSubscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repeaterTalkGroupSubscription` (
  `repeaterId` int(11) NOT NULL DEFAULT '0',
  `talkGroups` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`repeaterId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `repeaters`
--

DROP TABLE IF EXISTS `repeaters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repeaters` (
  `repeaterId` int(11) NOT NULL DEFAULT '0',
  `callsign` varchar(50) DEFAULT NULL,
  `lat` decimal(10,8) DEFAULT NULL,
  `lng` decimal(11,8) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `lastUpdated` bigint(20) DEFAULT NULL,
  `TXFrequency` float(7,4) DEFAULT NULL,
  `RXFrequency` float(7,4) DEFAULT NULL,
  `ColorCode` tinyint(4) DEFAULT NULL,
  `HardWare` varchar(50) DEFAULT NULL,
  `Firmware` varchar(50) DEFAULT NULL,
  `location` varchar(32) DEFAULT NULL,
  `website` varchar(64) DEFAULT NULL,
  `outageTextSent` tinyint(1) DEFAULT '0',
  `priomsg` varchar(128) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`repeaterId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER addRepeaterData
AFTER INSERT ON repeaters
FOR EACH ROW
BEGIN
INSERT INTO repeaterRegionSubscriptions VALUES (new.repeaterId, (SELECT SUM(region) FROM regions));
INSERT INTO repeaterTalkGroupSubscription VALUES (new.repeaterId,(SELECT SUM(talkGroupId) FROM talkGroups));
INSERT INTO repeaterAllowReflector VALUES (new.repeaterId, 1);
INSERT INTO repeaterAllowOnDemand VALUES (new.repeaterId, 1);
INSERT INTO activityLogs VALUES ('',3,null,new.repeaterId,null,'::1',now());
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER deleteRepeaterData AFTER DELETE on repeaters
FOR EACH ROW
BEGIN
DELETE FROM repeaterSysops WHERE repeaterSysops.repeaterId=OLD.repeaterId;
DELETE FROM repeaterAllowOnDemand WHERE repeaterAllowOnDemand.repeaterId=OLD.repeaterId;
DELETE FROM repeaterAllowReflector WHERE repeaterAllowReflector.repeaterId=OLD.repeaterId;
DELETE FROM repeaterGroupSubscription WHERE repeaterGroupSubscription.repeaterId=OLD.repeaterId;
DELETE FROM repeaterTalkGroupSubscription WHERE repeaterTalkGroupSubscription.repeaterId=OLD.repeaterId;
DELETE FROM repeaterRegionSubscriptions WHERE repeaterRegionSubscriptions.repeaterId=OLD.repeaterId;
DELETE FROM groups WHERE (SELECT COUNT(*) FROM repeaterGroupSubscription WHERE repeaterGroupSubscription.member = groups.groupId)=0;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `sysops`
--

DROP TABLE IF EXISTS `sysops`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sysops` (
  `sysopId` int(11) NOT NULL AUTO_INCREMENT,
  `callsign` varchar(16) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `isAdmin` tinyint(1) NOT NULL DEFAULT '0',
  `cellphone` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`sysopId`,`callsign`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `talkGroups`
--

DROP TABLE IF EXISTS `talkGroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `talkGroups` (
  `dmrId` int(11) NOT NULL DEFAULT '0',
  `talkGroupId` tinyint(4) DEFAULT NULL,
  `talkGroupName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`dmrId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userRegistration`
--

DROP TABLE IF EXISTS `userRegistration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userRegistration` (
  `dmrId` int(11) NOT NULL DEFAULT '0',
  `repeaterId` int(11) DEFAULT NULL,
  `time` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`dmrId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-28 12:57:11