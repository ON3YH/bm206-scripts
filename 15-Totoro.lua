-- Core modules
require("Core")
require("Tools")

--[[

  Project Totoro
  Port of 2061-BE/2621-DL management system for BrandMeister DMR Server

  Original code was written by Wim ON4AKH
  Web front-end application by Yentel ON3YH
  Ported to new plug-in system by Artem R3ABM

  Special thanks to Rudy PD0ZRY

]]--

local Totoro = { }

--[[

 Common variables
 
]]--

local bit = require("bit")

local client = require("memcached")
local cache = client.connect()

local driver = require("luasql.mysql")
local environment = assert(driver.mysql())
local conn = assert (environment:connect('bmconfig', 'bmconfig', 'bm@config'))

local map = readReflectorMapFromFile("Data/reflector.db")

local json = require("dkjson")
local configuration = json.decode(readFile("Data/generic.json"))
local timeout = configuration["Timers"]["OnDemand"]

local repeaters = nil
local regions = nil
local repeaterRegionSubscriptions = nil
local repeaterGroup = nil
local talkGroups = nil
local repeaterTalkGroupSubscriptions = nil
local repeaterAllowReflector = nil
local repeaterAllowOnDemand = nil
local onDemandGroups = nil

map[4000] = 0
map[5000] = 0

--[[

 Import from ToolsON.lua
 
]]--

local function loadRegions()
  local map = { }
  local cursor, errorString = conn:execute([[select dmrId, region, regionName from regions]])
  local row = cursor:fetch({ }, "a")
  while row do
    map[tonumber(row.dmrId)] = tonumber(row.region)
    row = cursor:fetch(row, "a")
  end
  cursor:close()
  return map
end

local function getRegionSubscriptions()
  local map = { }
  local cursor, errorString = conn:execute([[select repeaterId, regions from repeaterRegionSubscriptions]])
  local row = cursor:fetch({ }, "a")
  while row do
    map[tonumber(row.repeaterId)] = tonumber(row.regions)
    row = cursor:fetch(row, "a")
  end
  cursor:close()
  return map
end
  
local function getGroupSubscriptions()
  local map = { }
  local cursor, errorString = conn:execute([[select repeaterId, member from repeaterGroupSubscription]])
  local row = cursor:fetch ({ }, "a")
  while row do
    map[tonumber(row.repeaterId)] = tonumber(row.member)
    row = cursor:fetch(row, "a")
  end
  cursor:close()
  return map
end

local function loadTalkGroups()
  local map = { }
  local cursor, errorString = conn:execute([[select dmrId, talkGroupId, talkGroupName from talkGroups]])
  local row = cursor:fetch({ }, "a")
  while row do
    map[tonumber(row.dmrId)] = tonumber(row.talkGroupId)
    row = cursor:fetch(row, "a")
  end
  cursor:close()
  return map
end

local function loadOnDemandGroups()
  local map = { }
  local cursor, errorString = conn:execute([[select dmrId, timeSlot, type from onDemandGroups]])
  local row = cursor:fetch ({ }, "a")
  while row do
    local info = { ["timeSlot"] = tonumber(row.timeSlot), ["type"] = tonumber(row.type) }
    map[tonumber(row.dmrId)] = info
    row = cursor:fetch(row, "a")
  end
  cursor:close()
  return map
end

local function getTalkGroupSubscriptions()
  local map = { }
  local cursor, errorString = conn:execute([[select repeaterId, talkGroups from repeaterTalkGroupSubscription]])
  local row = cursor:fetch({ }, "a")
  while row do
    map[tonumber(row.repeaterId)] = tonumber(row.talkGroups)
    row = cursor:fetch(row, "a")
  end
  cursor:close()
  return map
end

local function getRepeaterReflector()
  local map = { }
  local cursor, errorString = conn:execute([[select repeaterId, allow from repeaterAllowReflector]])
  local row = cursor:fetch({ }, "a")
  while row do
    map[tonumber(row.repeaterId)] = tonumber(row.allow)
    row = cursor:fetch(row, "a")
  end
  cursor:close()
  return map
end

local function getRepeaterOnDemand()
  local map = { }
  local cursor, errorString = conn:execute([[select repeaterId, allow from repeaterAllowOnDemand]])
  local row = cursor:fetch({ }, "a")
  while row do
    map[tonumber(row.repeaterId)] = tonumber(row.allow)
    row = cursor:fetch(row, "a")
  end
  cursor:close()
 return map
end

local function getRepeaters()
  local map = { }
  local cursor, errorString = conn:execute([[select repeaterId, callsign from repeaters]])
  local row = cursor:fetch({ }, "a")
  while row do
    map[tonumber(row.repeaterId)] = row.callsign
    row = cursor:fetch(row, "a")
  end
  cursor:close()
  return map
end

--[[

 Import from ON.lua
 
]]--

local function loadFromDatabase()
  repeaters = getRepeaters()
  regions = loadRegions()
  repeaterRegionSubscriptions = getRegionSubscriptions()
  repeaterGroup = getGroupSubscriptions()
  talkGroups = loadTalkGroups()
  repeaterTalkGroupSubscriptions = getTalkGroupSubscriptions()
  repeaterAllowReflector = getRepeaterReflector()
  repeaterAllowOnDemand = getRepeaterOnDemand()
  onDemandGroups = loadOnDemandGroups()
  report("[Totoro] Finished reloading tables!")
  cache:delete("DatabaseUpdate", false)
end

local function reloadFromDatabase()
  if (cache:get("repeatersUpdate") ~= nil)
  then
    repeaters = getRepeaters()
    report("[Totoro] repeaters table reloaded from database!")
    cache:delete("repeatersUpdate", false)
  end
  if (cache:get("regionsUpdate") ~= nil)
  then
    regions = loadRegions()
    report("[Totoro] regions table reloaded from database!")
    cache:delete("regionsUpdate", false)
  end
  if (cache:get("regionsSubscriptionUpdate") ~= nil)
  then
    repeaterRegionSubscriptions = getRegionSubscriptions()
    report("[Totoro] repeaterRegionSubscriptions  table reloaded from database!")
    cache:delete("regionsSubscriptionUpdate", false)
  end
  if (cache:get("groupSubscriptionsUpdate") ~= nil)
  then
    repeaterGroup = getGroupSubscriptions()
    report("[Totoro] repeaterGroupSubscription table reloaded from database!")
    cache:delete("groupSubscriptionsUpdate", false)
  end
  if (cache:get("talkgroupsUpdate") ~= nil)
  then
    talkGroups = loadTalkGroups()
    report("[Totoro] talkGroups table reloaded from database!")
    cache:delete("talkgroupsUpdate", false)
  end
  if (cache:get("talkgroupSubscriptionsUpdate") ~= nil)
  then
    repeaterTalkGroupSubscriptions = getTalkGroupSubscriptions()
    report("[Totoro] repeaterTalkGroupSubscription table reloaded from database!")
    cache:delete("talkgroupSubscriptionsUpdate", false)
  end
  if (cache:get("allowReflectorsUpdate") ~= nil)
  then
    repeaterAllowReflector = getRepeaterReflector()
    report("[Totoro] repeaterAllowReflector table reloaded from database!")
    cache:delete("allowReflectorsUpdate", false)
  end
  if (cache:get("allowOnDemandUpdate") ~= nil)
  then
    repeaterAllowOnDemand = getRepeaterOnDemand()
    report("[Totoro] repeaterAllowOnDemand table reloaded from database!")
    cache:delete("allowOnDemandUpdate", false)
  end
  if (cache:get("onDemandGroupsUpdate") ~= nil)
  then
    onDemandGroups = loadOnDemandGroups()
    report("[Totoro] onDemandGroups table reloaded from database!")
    cache:delete("onDemandGroupsUpdate", false)
  end
  cache:delete("DatabaseUpdate", false)
end

local function canRoute(number, destination, slot)
  local stringJson = cache:get("lastTg-" .. number)
  if (stringJson ~= nil) then
    local data = json.decode(stringJson)
    if (data["dest"] ~= destination) and (data["timeSlot"] == slot) then
      report("ROUTE NOT ALLOWED Repeater: " .. number .. " dest: " .. destination .. " slot: " .. slot .. " time out not expired")
      return false
    end
  end
  local tbl = { dest = destination, timeSlot = slot }
  local jsonString = json.encode(tbl, { indent = false })
  cache:set("lastTg-" .. number, jsonString, 600)
  return true
end

local function routeToFastForward(kind, name, number, slot, flavor, source, destination, map)
  if
    (kind ~= LINK_TYPE_NETWORK) and (destination ~= 9) and
    (map[destination] == nil) and -- Do not forward reflector ID's
    (validateGeographicRegionCode(destination, "8") or
     validateGeographicRegionCode(destination, "9") or
     getCountryCode(destination))
  then
    report("Route to FastForeward: " .. destination)
    local contexts = getContextTable()
    for _, parameters in pairs(contexts) do
      if parameters.name == "FastForward"
      then
        newRoute(parameters.object, 0, 0)
      end
    end
  end
end

local function routeToRepeaters(kind, name, number, slot, flavor, source, destination, map)
  local contexts = getContextTable()
  local region = regions[destination]
  local talkGroup = nil
  local sourceRepeaterGroup = nil

  -- Is this a call to any of the national regions?
  if (region ~= nil) then
    -- If the source is a repeater, get its region subscriptions
    if (kind == LINK_TYPE_REPEATER) then
      local sourceRepeaterRegions = repeaterRegionSubscriptions[number]
      -- Is this source itself in the region? If not, create an on demand route
      if (sourceRepeaterRegions ~= nil) then
        -- report("[routeToRepeaters] Source(" .. kind .. "-" .. number .. ") subscribed to regions: " .. sourceRepeaterRegions)
        if (bit.band(sourceRepeaterRegions, region) == 0) then
          report("[Totoro] Repeater " .. number .. " is not subscribed to region " .. sourceRepeaterRegions .. ", making an on demand route!")
          makeOnDemandRouteForGroup(kind, number, 1, destination, source, timeout)
        end
      -- Source has no region subscriptions
      else
        report("[Totoro] ERROR! " .. number .. " is not subscribed to any regions!")
      end
    end
    routeToFastForward(kind, name, number, slot, flavor, source, destination, map)
    slot = 1
  else
    -- It's not a call to any of the national regions
    -- if dest = 9, See if this is a repeater and it belongs to a group
    if (kind == LINK_TYPE_REPEATER) and (destination == 9) then
      sourceRepeaterGroup = repeaterGroup[number]
      if (sourceRepeaterGroup ~= nil) and (sourceRepeaterGroup ~= 0) then
        report("[Totoro] Forwarding data on repeater " .. number .. " slot " .. kind+1 .. " to cluster: " .. sourceRepeaterGroup)
      end
    end

    -- See if this is one of the talkgroups a repeater can have a subscription on
    talkGroup = talkGroups[destination]
    if (talkGroup ~= nil) then
      if (kind == LINK_TYPE_REPEATER) then
        local sourceRepeaterTalkGroups = repeaterTalkGroupSubscriptions[number]
        if (bit.band(sourceRepeaterTalkGroups, talkGroup) == 0) then
          report("[Totoro] Blocked usage of TG " .. destination .. " on repeater " .. number .. " slot " .. kind+1 .. " for user " .. source)
        else
          report("[Totoro] Forwarding data for TG " .. destination .. " on repeater " .. number .. " slot " .. kind+1 .. " from user " .. source)
          routeToFastForward(kind, name, number, slot, flavor, source, destination, map)
        end
      end
    end

    -- See if this is an ondemand group and on demand is allowed on this repeater
    local info = onDemandGroups[destination]
    if (info ~= nil) then --Only for defined ondemand talkgroups
      if (kind == LINK_TYPE_REPEATER) and ((repeaterAllowOnDemand[number] == 1) or (info["type"] == 2)) then
        makeOnDemandRouteForGroup(kind, number, info["timeSlot"], destination, source, timeout)
        routeToFastForward(kind, name, number, slot, flavor, source, destination, map)
      end
    end
    slot = 2
  end

  -- Loop through the repeaters to see if they have subscriptions
  for _, parameters in pairs(contexts) do
    if
      -- Do not route call back
      ((kind ~= parameters.kind) or
       (number ~= parameters.number)) and
      -- Remote should be a repeater
      validateRegularRepeater(parameters)
    then
      local routed = false
      -- Is this repeater in an ondemand group for this destination and slot
      local onDemandInfo = cache:get("onDemand-" .. parameters.number)
      if (onDemandInfo ~= nil) then
        local data = json.decode(onDemandInfo)
        if (destination == data["dest"]) then
          if (canRoute(parameters.number, destination, data["timeSlot"])) then
            newRoute(parameters.object, data["timeSlot"], 0)
          end
          routed = true -- Moet dit hier niet in de if staan hierboven ?
        end
      end

      -- Is the repeater subscribed to a region ?
      if (region ~= nil) and (not routed) then
        local repeaterRegions = repeaterRegionSubscriptions[parameters.number]
        if (repeaterRegions ~= nil) and
           (bit.band(repeaterRegions, region) ~= 0) and
           (canRoute(parameters.number, destination, 1))
        then
          newRoute(parameters.object, 1, 0)
        end
      else
        -- Does this repeater belong to a group ?
        if (sourceRepeaterGroup ~= nil) and (sourceRepeaterGroup ~= 0) and (not routed) then
          local destRepeaterGroup = repeaterGroup[parameters.number]
          if (destRepeaterGroup ~= nil) and
             (sourceRepeaterGroup == destRepeaterGroup) and
             (canRoute(parameters.number, destination, 2))
          then
            newRoute(parameters.object, 2, 0)
          end
        end
        -- Is this repeater subscribed to a talkgroup
        if (talkGroup ~= nil) and (not routed) then
          local repeaterTalkGroups = repeaterTalkGroupSubscriptions[parameters.number]
          if (repeaterTalkGroups ~= nil) and
             (bit.band(repeaterTalkGroups, talkGroup) ~= 0) and
             (canRoute(parameters.number, destination, 2))
          then
            newRoute(parameters.object, 2, 0)
          end
        end
      end
    end
  end
end

-- Force loadFromDatabase() after the BrandMeister application has started
loadFromDatabase()

--[[

 Import from FilterON.lua

]]--

local function filterCallSession(kind, name, number, slot, flavor, source, destination)
  -- [Rule 1] All Belgium national and regional calls only on TS1
  local region = regions[destination]
  if (slot == 1) and (region == nil) then
    report("**BLOCKED** traffic. Kind:" .. kind .. " name:" .. name .. " slot:" .. slot .. " flavor:" .. flavor .. " source:" .. source .. " dest:" .. destination)
    return true
  end

  -- [Rule 2] All other talkgroups on TS2
  if (slot == 2) and (region ~= nil) then
    report("**BLOCKED** traffic. Kind:" .. kind ..  " name:" .. name .. " slot:" .. slot .. " flavor:" .. flavor .. " source:" .. source .. " dest:" .. destination)
    return true
  end

  -- [Rule 3] Sysops can block the usage of reflectors
  if (destination >= 4000 and destination <= 5000) and (kind == LINK_TYPE_REPEATER) and (slot == 2) then
    if (repeaterAllowReflector[number] == nil) or -- Deny because the sysop wants this to be denied
       (map[destination] == nil)                  -- Deny because the reflector is not in reflector.db
    then
      report("[Totoro] Blocked usage of reflector " .. destination .. " on repeater " .. number .. " for user " .. source)
      return true  -- Block usage of reflector
    else
      if (destination ~= 4000 and destination ~=5000) then
        cache:set("reflector-" .. number, destination, timeout)
        report("[Totoro] Connecting repeater " .. number .. " to reflector " .. destination .. " as requested by " .. source)
      end
      if (destination == 4000) then
        cache:delete("reflector-" .. number, false)
        if (cache:get("onDemand-" .. number) ~= nil) then
          cache:delete("onDemand-" .. number, false)
          return true
        end
      end
      if (destination == 5000) and
         (cache:get("reflector-" .. number) == nil)
      then
        return true
      end
    end
  end

  -- Accept all calls by default
  return false
end

--[[

 Import from RegistryON.lua

]]--

function Totoro.handleCallSession(kind, name, number, slot, flavor, source, destination)

  if filterCallSession(kind, name, number, slot, flavor, source, destination)
  then
    return REGISTRY_STOP_APPENDING
  end

  -- Check for database updates by reading the memcached keys
  if (cache:get("DatabaseUpdate")) ~= nil
  then
    report("[Totoro] Database changed, reloading changed tables...")
    reloadFromDatabase()
  end

  -- If this is a group call
  if (bit.band(flavor, SESSION_TYPE_FLAG_GROUP) ~= 0)
  then
    routeToRepeaters(kind, name, number, slot, flavor, source, destination, map)
    -- makeRouteForReflector is replaced by 25-Dongle.lua
    -- makeRouteForReflector(kind, name, number, destination, map)
  end

  -- Default return value
  return REGISTRY_CONTINUE_APPENDING
end

--[[

 Extra part of plug-in

]]--

function Totoro.handleConfigurationEvent()
  report("Reloading configuration of Project Totoro...")
  reflectors = readReflectorMapFromFile("Data/reflector.db")
  reloadFromDatabase()
end

return Totoro
